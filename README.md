# Super Smash Bros Ultimate API for the win

TP réalisé en peer programming par :

-   Théo Helaine
-   Emmanuel Bosquet

On a abusé du potentiel de Doctrine et Twig et on a fait un CRUD en HTML.

## Comment faire tourner en local

### Prérequis

-   une instance de MySQSL ou MariaDB en local, qui écoute sur le port 3306
-   PHP version 8.0.2 ou plus
-   Composer 2.2.4
-   Symfony CLI 5.0.8 ou plus

### Procédure

Cloner le dépôt

    git clone https://gitlab.com/m7762/apissbu.git

Installer les dépendances

    composer install

Adapter la ligne `DATABASE_URL` dans le fichier `.env`, pour qu'elle corresponde à vos identifiants.

    DATABASE_URL="mysql://root:password@localhost:3306/ssbu"

Créer la base de donnée intitulée `ssbu`

    php bin/console doctrine:database:create

Lancer les migrations avec doctrine.

    php bin/console doctrine:migrations:migrate

Lancer Symfony en ligne de commande

    symfony server:start

### Vérifier le fonctionnement

Consulter [la page d'acceuil](http://localhost:8000).

| entité     | opération         | path                                         |
| ---------- | ----------------- | -------------------------------------------- |
| Univers    | consulter         | <http://localhost:8000/univers>              |
|            | créer             | <http://localhost:8000/univers/new>          |
|            | editer            | <http://localhost:8000/univers/{id}/edit>    |
|            | consulter un seul | <http://localhost:8000/univers/{id}>         |
| Personnage | consulter         | <http://localhost:8000/personnage>           |
|            | créer             | <http://localhost:8000/personnage/new>       |
|            | editer            | <http://localhost:8000/personnage/{id}/edit> |
|            | consulter un seul | <http://localhost:8000/personnage/{id}>      |

et ainsi de suite, <http://localhost:8000/combat> et <http://localhost:8000/coup/special>

### Créer un personnage, ça plante

<http://localhost:8000/personnage/new> donne cette erreur:

    Object of class App\Entity\Univers could not be converted to string

On imagine que l'id d'Univers, étant un int, ne passe pas dans doctrine qui veut une string. On a tenté d'écrire une migration pour transformer l'id en Uuid mais, comme l'indique le commit `bdafed1` :

    N bdafed1 - Revert du commit de l'Uuid parce qu'on est des taches.

## Notes de dev

Après avoir tapé

    composer require orm

On nous dit:

```
doctrine/doctrine-bundle  instructions:

  * Modify your DATABASE_URL config in .env

  * Configure the driver (postgresql) and
    server_version (13) in config/packages/doctrine.yaml
```
