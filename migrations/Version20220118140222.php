<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220118140222 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE combat (id INT AUTO_INCREMENT NOT NULL, personnage_joueur_id INT NOT NULL, personnage_adversaire_id INT NOT NULL, date DATETIME NOT NULL, a_gagner TINYINT(1) DEFAULT NULL, INDEX IDX_8D51E39898FB72AC (personnage_joueur_id), INDEX IDX_8D51E3985D66515A (personnage_adversaire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE coup_special (id INT AUTO_INCREMENT NOT NULL, personnage_id INT NOT NULL, name VARCHAR(255) NOT NULL, pourcentage VARCHAR(255) NOT NULL, direction VARCHAR(255) NOT NULL, INDEX IDX_5AF796975E315342 (personnage_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE personnage (id INT AUTO_INCREMENT NOT NULL, univers_id INT NOT NULL, name VARCHAR(255) NOT NULL, link_image VARCHAR(1000) NOT NULL, INDEX IDX_6AEA486D1CF61C0B (univers_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE univers (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, link_image VARCHAR(1000) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE combat ADD CONSTRAINT FK_8D51E39898FB72AC FOREIGN KEY (personnage_joueur_id) REFERENCES personnage (id)');
        $this->addSql('ALTER TABLE combat ADD CONSTRAINT FK_8D51E3985D66515A FOREIGN KEY (personnage_adversaire_id) REFERENCES personnage (id)');
        $this->addSql('ALTER TABLE coup_special ADD CONSTRAINT FK_5AF796975E315342 FOREIGN KEY (personnage_id) REFERENCES personnage (id)');
        $this->addSql('ALTER TABLE personnage ADD CONSTRAINT FK_6AEA486D1CF61C0B FOREIGN KEY (univers_id) REFERENCES univers (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE combat DROP FOREIGN KEY FK_8D51E39898FB72AC');
        $this->addSql('ALTER TABLE combat DROP FOREIGN KEY FK_8D51E3985D66515A');
        $this->addSql('ALTER TABLE coup_special DROP FOREIGN KEY FK_5AF796975E315342');
        $this->addSql('ALTER TABLE personnage DROP FOREIGN KEY FK_6AEA486D1CF61C0B');
        $this->addSql('DROP TABLE combat');
        $this->addSql('DROP TABLE coup_special');
        $this->addSql('DROP TABLE personnage');
        $this->addSql('DROP TABLE univers');
    }
}
