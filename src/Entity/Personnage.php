<?php

namespace App\Entity;

use App\Repository\PersonnageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PersonnageRepository::class)]
class Personnage
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'string', length: 1000)]
    private $link_image;

    #[ORM\ManyToOne(targetEntity: Univers::class, inversedBy: 'personnages')]
    #[ORM\JoinColumn(nullable: false)]
    private $univers;

    #[ORM\OneToMany(mappedBy: 'personnage', targetEntity: CoupSpecial::class)]
    private $coupSpecials;

    #[ORM\OneToMany(mappedBy: 'personnageJoueur', targetEntity: Combat::class)]
    private $combats;

    public function __construct()
    {
        $this->coupSpecials = new ArrayCollection();
        $this->combats = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLinkImage(): ?string
    {
        return $this->link_image;
    }

    public function setLinkImage(string $link_image): self
    {
        $this->link_image = $link_image;

        return $this;
    }

    public function getUnivers(): ?Univers
    {
        return $this->univers;
    }

    public function setUnivers(?Univers $univers): self
    {
        $this->univers = $univers;

        return $this;
    }

    /**
     * @return Collection|CoupSpecial[]
     */
    public function getCoupSpecials(): Collection
    {
        return $this->coupSpecials;
    }

    public function addCoupSpecial(CoupSpecial $coupSpecial): self
    {
        if (!$this->coupSpecials->contains($coupSpecial)) {
            $this->coupSpecials[] = $coupSpecial;
            $coupSpecial->setPersonnage($this);
        }

        return $this;
    }

    public function removeCoupSpecial(CoupSpecial $coupSpecial): self
    {
        if ($this->coupSpecials->removeElement($coupSpecial)) {
            // set the owning side to null (unless already changed)
            if ($coupSpecial->getPersonnage() === $this) {
                $coupSpecial->setPersonnage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Combat[]
     */
    public function getCombats(): Collection
    {
        return $this->combats;
    }

    public function addCombat(Combat $combat): self
    {
        if (!$this->combats->contains($combat)) {
            $this->combats[] = $combat;
            $combat->setPersonnageJoueur($this);
        }

        return $this;
    }

    public function removeCombat(Combat $combat): self
    {
        if ($this->combats->removeElement($combat)) {
            // set the owning side to null (unless already changed)
            if ($combat->getPersonnageJoueur() === $this) {
                $combat->setPersonnageJoueur(null);
            }
        }

        return $this;
    }
}
