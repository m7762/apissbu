<?php

namespace App\Entity;

use App\Repository\CombatRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CombatRepository::class)]
class Combat
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'datetime')]
    private $date;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $aGagner;

    #[ORM\ManyToOne(targetEntity: Personnage::class, inversedBy: 'combats')]
    #[ORM\JoinColumn(nullable: false)]
    private $personnageJoueur;

    #[ORM\ManyToOne(targetEntity: Personnage::class, inversedBy: 'combats')]
    #[ORM\JoinColumn(nullable: false)]
    private $personnageAdversaire;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getAGagner(): ?bool
    {
        return $this->aGagner;
    }

    public function setAGagner(?bool $aGagner): self
    {
        $this->aGagner = $aGagner;

        return $this;
    }

    public function getPersonnageJoueur(): ?Personnage
    {
        return $this->personnageJoueur;
    }

    public function setPersonnageJoueur(?Personnage $personnageJoueur): self
    {
        $this->personnageJoueur = $personnageJoueur;

        return $this;
    }

    public function getPersonnageAdversaire(): ?Personnage
    {
        return $this->personnageAdversaire;
    }

    public function setPersonnageAdversaire(?Personnage $personnageAdversaire): self
    {
        $this->personnageAdversaire = $personnageAdversaire;

        return $this;
    }
}
