<?php

namespace App\Controller;

use App\Entity\Combat;
use App\Form\CombatType;
use App\Repository\CombatRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/combat')]
class CombatController extends AbstractController
{
    // get method on root_url/combat/
    #[Route('/', name: 'combat_index', methods: ['GET'])]
    public function index(CombatRepository $combatRepository): Response
    {
        return $this->render('combat/index.html.twig', [
            'combats' => $combatRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'combat_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $combat = new Combat();
        $form = $this->createForm(CombatType::class, $combat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($combat);
            $entityManager->flush();

            return $this->redirectToRoute('combat_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('combat/new.html.twig', [
            'combat' => $combat,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'combat_show', methods: ['GET'])]
    public function show(Combat $combat): Response
    {
        return $this->render('combat/show.html.twig', [
            'combat' => $combat,
        ]);
    }

    #[Route('/{id}/edit', name: 'combat_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Combat $combat, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CombatType::class, $combat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('combat_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('combat/edit.html.twig', [
            'combat' => $combat,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'combat_delete', methods: ['POST'])]
    public function delete(Request $request, Combat $combat, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$combat->getId(), $request->request->get('_token'))) {
            $entityManager->remove($combat);
            $entityManager->flush();
        }

        return $this->redirectToRoute('combat_index', [], Response::HTTP_SEE_OTHER);
    }
}
