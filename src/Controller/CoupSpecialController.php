<?php

namespace App\Controller;

use App\Entity\CoupSpecial;
use App\Form\CoupSpecialType;
use App\Repository\CoupSpecialRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/coup/special')]
class CoupSpecialController extends AbstractController
{
    #[Route('/', name: 'coup_special_index', methods: ['GET'])]
    public function index(CoupSpecialRepository $coupSpecialRepository): Response
    {
        return $this->render('coup_special/index.html.twig', [
            'coup_specials' => $coupSpecialRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'coup_special_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $coupSpecial = new CoupSpecial();
        $form = $this->createForm(CoupSpecialType::class, $coupSpecial);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($coupSpecial);
            $entityManager->flush();

            return $this->redirectToRoute('coup_special_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('coup_special/new.html.twig', [
            'coup_special' => $coupSpecial,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'coup_special_show', methods: ['GET'])]
    public function show(CoupSpecial $coupSpecial): Response
    {
        return $this->render('coup_special/show.html.twig', [
            'coup_special' => $coupSpecial,
        ]);
    }

    #[Route('/{id}/edit', name: 'coup_special_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, CoupSpecial $coupSpecial, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CoupSpecialType::class, $coupSpecial);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('coup_special_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('coup_special/edit.html.twig', [
            'coup_special' => $coupSpecial,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'coup_special_delete', methods: ['POST'])]
    public function delete(Request $request, CoupSpecial $coupSpecial, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$coupSpecial->getId(), $request->request->get('_token'))) {
            $entityManager->remove($coupSpecial);
            $entityManager->flush();
        }

        return $this->redirectToRoute('coup_special_index', [], Response::HTTP_SEE_OTHER);
    }
}
